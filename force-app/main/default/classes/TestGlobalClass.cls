global class TestGlobalClass {
    global static void staticMethod(){
        System.debug('static method');
		System.debug('Second package');
        System.debug('sEcond Release');
    }
    global void nonStaticMethod(){
        System.debug('instance method');
    }
}